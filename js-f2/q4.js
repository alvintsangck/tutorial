// Write a JavaScript to show below:

// 1
// 2
// 4
// 5
// 7
// 8
// 10
// 11

// 0 to 11
for (let i = 0; i < 12; i++) {
  // skip multiple of 3
  if (i % 3 != 0) {
    console.log(i);
  }
}
