// Using process.argv, write a JavaScript to perform below:

// node q7.js 1,3,4,5,9,10,11,14
// The sum of the numbers is: 57

let numbers = process.argv[2]
// "1,2,3" -> [1,2,3]
let numberArr = numbers.split(",")
let result = 0
for (let i = 0; i < numberArr.length ; i ++) {
    // Number(numberArr[i])
    result += parseInt(numberArr[i])
}
console.log(result);