// Using process.argv, write a JavaScript to perform below:

// node q8.js elephant e
// Result: lphant

let word = process.argv[2];
let replacement = process.argv[3];

let text = "";

//  loop every character of word
for (let i = 0; i < word.length; i++) {
  if (word[i] != replacement) {
    text += word[i];
  }
}

// for (let char of word) {
//   if (char != replacement) {
//     text += char;
//   }
// }

console.log(`Result: ${text}`);

// regex
// const reg = new RegExp(replacement, "g");
// console.log(word.replace(reg, ""));

// console.log(word.replaceAll(replacement, ""));
