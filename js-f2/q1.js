// Try not to run below in JavaScript, can you tell which of the following are showing true?

let a = 1;
let b = 2;
let c = 3;

// 1 > 2 == false
console.log(a > b);

// 3 < 2 * 2 == true
console.log(c < b * 2);

d = b + a * 2;
// 2 + 1 * 2 > 3 == true
console.log(d > c);

a = 10;
// 10 % 2 == true
console.log(a % b == 0);

// (2 + 3) * 10 == '50' == true
// (2 + 3) * 10 === '50' == false
console.log("50" == (b + c) * a);

// Error
console.log(50 is (b + c) * a);

// Error
console.log(True or False);
