// Write a JavaScript to show below:

// +-+-+-+
//  -+-+-
//   +-+
//    -

let plus = "+";
let minus = "-";
let space = " ";
let maxRow = 4;
let symbolMaxLength = 2 * maxRow - 1;
let isPlus = maxRow % 2 == 0;

for (let row = 0; row < maxRow; row++) {
  let symbols = "";
  for (let i = 0; i < row; i++) {
    symbols += space;
  }
  for (let symbolColumn = row; symbolColumn < symbolMaxLength - row; symbolColumn++) {
    if (isPlus) {
      symbols += plus;
    } else {
      symbols += minus;
    }
    isPlus = !isPlus;
  }
  console.log(symbols);
}
