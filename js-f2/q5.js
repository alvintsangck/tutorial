// Write a JavaScript to show below:

//   *
//  ***
// *****

let star = "*";
let space = " ";
let maxRow = 3;
for (let i = 0; i < maxRow; i++) {
  let result = "";
  for (let j = 0; j < maxRow - 1 - i; j++) {
    result += space;
  }
  for (let k = 0; k < 2 * i + 1; k++) {
    result += "*";
  }
  console.log(result);
  // one liner
  // console.log(" ".repeat(maxRow - i - 1) + star.repeat(2 * i + 1));
}

// let star = "*";
// for (let i = 0; i < 3; i++) {
//   let spaces = "";
//   for (let j = 0; j < 2 - i; j++) {
//     spaces += " ";
//   }
//   if (i != 0) {
//     star += "**";
//   }
//   console.log(spaces + star);
// }
