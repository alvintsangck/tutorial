// Write and Javascript to calculate BMI and display the result

// Below 18.5 : Underweight
// 18.5 – 24.9 : Normal
// 25.0 – 29.9 : Overweight
// 30.0 and above : Obese

// Weight (kg): 88
// Height (m): 1.8

// BMI: 27
// Result: Overweight
