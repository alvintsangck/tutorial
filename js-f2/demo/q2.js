// Try not to run below in JavaScript, can you tell the type and value of variables a1, a2 and a3 below?

let x = 2;
let y = "hi";
let z = 5;

if (y[0].toUpperCase() + y[1] == "Hi") {
  a1 = y * x;
  a2 = parseInt(z + "");
} else {
  a3 = "True";
}

console.log(a1);
console.log("Type of a1: ", typeof a1);

console.log(a2);
console.log("Type of a2: ", typeof a2);
console.log(a3);
