// Write and Javascript to calculate BMI and display the result

// Below 18.5 : Underweight
// 18.5 – 24.9 : Normal
// 25.0 – 29.9 : Overweight
// 30.0 and above : Obese

// Weight (kg): 88
// Height (m): 1.8

// BMI: 27
// Result: Overweight

let weight = 88;
let height = 1.8;
// height ** 2 == Math.pow(height, 2);
let bmi = weight / height ** 2;

let result = "";

if (bmi < 18.5) {
  result = "Underweight";
  // no need bmi > 18.5 because it was guarded by the first if
} else if (bmi < 25) {
  result = "Normal";
} else if (bmi < 30) {
  result = "Overweight";
} else {
  result = "Obese";
}

console.log(`BMI: ${bmi}`);
//or console.log("BMI:  " + bmi);
console.log(`Result: ${result}`);
