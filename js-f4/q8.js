// Reuse the name collector, make a game. The result should be as follows:

// Enter player 1 name (enter nothing to stop): Alex
// Enter player 2 name (enter nothing to stop): Gordon
// Enter player 3 name (enter nothing to stop): Michael
// Enter player 4 name (enter nothing to stop):
// Here are the player names and their scores: Alex (0), Gordon (0), Michael (0)

// Alex's guess the number (1-6): 2
// Gordon's guess the number (1-6): 4
// Michael's guess the number (1-6): 5
// The result is: 5
// The scores: Alex (0), Gordon (0), Michael (1)

const readlineSync = require("readline-sync");

let players = [];

while (true) {
  let playerNumber = players.length + 1;
  let playerName = readlineSync.question(`Enter player ${playerNumber} name (enter nothing to stop):`);
  if (playerName == "") {
    break;
  } else {
    players.push({ name: playerName, scores: 0 });
    playerNumber++;
  }
}

console.log(`Here are the player names and their scores: ${printPlayerStatus(players)}`);

let result = 5;
for (let player of players) {
  let guess = readlineSync.question(`${player.name}'s guess the number (1-6): `);
  if (result == guess) {
    player.scores++;
  }
}

console.log("The result is: " + result);
console.log(`The scores: ${printPlayerStatus(players)}`);

function printPlayerStatus(players) {
  return players.reduce(function (prev, next) {
    // 1st prev == ""
    // 1st next == players[0]
    if (prev == "") {
      return (prev += `${next.name} (${next.scores})`);
    }
    return (prev += `, ${next.name} (${next.scores})`);
  }, "");
}
