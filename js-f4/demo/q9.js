// Reuse the name collector and the game, turn them into function(s).
// Then redo the game with three rounds and record them via fs and JSON to a result.json file.

// Enter player 1 name (enter nothing to stop): Alex
// Enter player 2 name (enter nothing to stop): Gordon
// Enter player 3 name (enter nothing to stop): Michael
// Enter player 4 name (enter nothing to stop):
// Here are the player names and their scores: Alex (0), Gordon (0), Michael (0)
// == Round 1 ==
// Alex's guess the number (Big, Small): Big
// Gordon's guess the number (Big, Small): Big
// Michael's guess the number (Big, Small): Small
// The result is: 4
// The scores: Alex (1), Gordon (1), Michael (0)
// == Round 2 ==
// Alex's guess the number (Big, Small): Big
// Gordon's guess the number (Big, Small): Big
// Michael's guess the number (Big, Small): Small
// The result is: 2
// The scores: Alex (1), Gordon (1), Michael (1)
// == Round 3 ==
// Alex's guess the number (Big, Small): Big
// Gordon's guess the number (Big, Small): Big
// Michael's guess the number (Big, Small): Small
// The result is: 5
// The scores: Alex (2), Gordon (2), Michael (1)"
