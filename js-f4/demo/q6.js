// Using readline-sync, make a name collector. The result should be as follows:

// Enter player 1 name (enter nothing to stop): Alex
// Enter player 2 name (enter nothing to stop): Gordon
// Enter player 3 name (enter nothing to stop): Michael
// Enter player 4 name (enter nothing to stop):
// Here are the player names: Alex, Gordon, Michael

// https://www.npmjs.com/package/readline-sync