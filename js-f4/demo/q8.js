// Reuse the name collector, make a game. The result should be as follows:

// Enter player 1 name (enter nothing to stop): Alex
// Enter player 2 name (enter nothing to stop): Gordon
// Enter player 3 name (enter nothing to stop): Michael
// Enter player 4 name (enter nothing to stop):
// Here are the player names and their scores: Alex (0), Gordon (0), Michael (0)

// Alex's guess the number (1-6): 2
// Gordon's guess the number (1-6): 4
// Michael's guess the number (1-6): 5
// The result is: 5
// The scores: Alex (0), Gordon (0), Michael (1)
