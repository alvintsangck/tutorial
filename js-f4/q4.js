// Cont'd of the last question, how do you get information from the below data?

let data = {
  routes: [
    {
      overnightRoute: 0,
      routeId: "2",
      routeName_c: "大澳 > 梅窩碼頭",
      routeName_e: "Tai O > Mui Wo Ferry Pier",
      routeName_s: "大澳 > 梅窝码头",
      routeNo: "1",
      specialRoute: 0,
    },
    {
      overnightRoute: 0,
      routeId: "1",
      routeName_c: "梅窩碼頭 > 大澳",
      routeName_e: "Mui Wo Ferry Pier > Tai O",
      routeName_s: "梅窝码头 > 大澳",
      routeNo: "1",
      specialRoute: 0,
    },
    {
      overnightRoute: 0,
      routeId: "3",
      routeName_c: "紅磡 (紅鸞道)  > 昂坪",
      routeName_e: "Hung Hom (Hung Luen Road)  > Ngong Ping",
      routeName_s: "红磡 (红鸾道)  > 昂坪",
      routeNo: "1R",
      specialRoute: 0,
    },
  ],
};

let routes = data.routes;

// 1. All the route numbers
let routeNumbers = [];
for (let i = 0; i < routes.length; i++) {
  routeNumbers.push(routes[i].routeNo);
}
console.log(routeNumbers);
// Bonus
// map
console.log(
  routes.map(function (route) {
    return route.routeNo;
  })
);
// arrow function
// console.log(routes.map((route) => route.routeNo););

// reduce
console.log(
  routes.reduce((prev, next) => {
    if (prev.routeNo) {
      return (prev.routeNo += `, ${next.routeNo}`);
    } else {
      return (prev += `, ${next.routeNo}`);
    }
  })
);
//chain with Array.reduce
// let reduceRouteNos = routes
//   .map((route) => route.routeNo)
//   .reduce((prevRouteNo, nextRouteNo) => {
//     return (prevRouteNo += `, ${nextRouteNo}`);
//   });
// console.log(reduceRouteNos);

// 2. All the terminal names
let terminalNames = [];
for (let route of routes) {
  let name = route.routeName_c;
  let nameArr = name.split(">");
  for (let name of nameArr) {
    terminalNames.push(name.trim());
  }
}
console.log(terminalNames);
// bonus
// map
let mapTerminalNames = routes.map((route) => route.routeName_c);
console.log(mapTerminalNames);
// reduce
console.log(mapTerminalNames.reduce((prev, next) => (prev += `, ${next}`)));

// Bonus: Use .map() and .reduce() to find the above