// Make a name + score collector. The result should be as follows:

// Enter player 1 name (enter nothing to stop): Alex
// Enter player 2 name (enter nothing to stop): Gordon
// Enter player 3 name (enter nothing to stop): Michael
// Enter player 4 name (enter nothing to stop):
// Here are the player names and their scores: Alex (0), Gordon (0), Michael (0)

const readlineSync = require("readline-sync");

let isNaming = true;
let players = [];
let playerNumber = players.length + 1;

while (isNaming) {
  let playerName = readlineSync.question(`Enter player ${playerNumber} name (enter nothing to stop):`);
  if (playerName == "") {
    isNaming = false;
  } else {
    players.push({ name: playerName, scores: 0 });
    playerNumber++;
  }
}

let text = "";
for (let i = 0; i < players.length; i++) {
  let player = players[i];
  let name = player.name;
  let scores = player.scores;
  text = text + `${name} ${scores} `;
}

// let playersText = players.reduce(function (prev, next) {
//   // 1st prev == ""
//   // 1st next == players[0]
//   if (prev == "") {
//     return (prev += `${next.name} (${next.scores})`);
//   }
//   return (prev += `, ${next.name} (${next.scores})`);
// }, "");

console.log(`Here are the player names: ${text}`);
// console.log(
//   `Here are the player names and their scores: ${players
//     .map((player) => `${player.name} (${player.score})`)
//     .join(", ")}`
// );
