// Can you create a proper structure to store below information?

// Peak: Tai Mo Shan
// Height: 957
// Peak: Lantau Peak
// Height: 934
// Peak: Sunset Peak
// Height: 869

let obj = [
  { peak: "Tai Mo Shan", height: 957 },
  { peak: "Lantau peak", height: 934 },
  { peak: "Sunset Peak", height: 869 },
];

console.log(obj);
