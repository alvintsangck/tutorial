// Reuse the name collector and the game, turn them into function(s).
// Then redo the game with three rounds and record them via fs and JSON to a result.json file.

// Enter player 1 name (enter nothing to stop): Alex
// Enter player 2 name (enter nothing to stop): Gordon
// Enter player 3 name (enter nothing to stop): Michael
// Enter player 4 name (enter nothing to stop):
// Here are the player names and their scores: Alex (0), Gordon (0), Michael (0)
// == Round 1 ==
// Alex's guess the number (Big, Small): Big
// Gordon's guess the number (Big, Small): Big
// Michael's guess the number (Big, Small): Small
// The result is: 4
// The scores: Alex (1), Gordon (1), Michael (0)
// == Round 2 ==
// Alex's guess the number (Big, Small): Big
// Gordon's guess the number (Big, Small): Big
// Michael's guess the number (Big, Small): Small
// The result is: 2
// The scores: Alex (1), Gordon (1), Michael (1)
// == Round 3 ==
// Alex's guess the number (Big, Small): Big
// Gordon's guess the number (Big, Small): Big
// Michael's guess the number (Big, Small): Small
// The result is: 5
// The scores: Alex (2), Gordon (2), Michael (1)"

// https://nodejs.org/api/fs.html#fswritefilefile-data-options-callback
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify

const readlineSync = require("readline-sync");
const fs = require("fs");

main();

function main() {
  let dataObject = {};
  let players = createPlayer(dataObject);
  for (let i = 0; i < 3; i++) {
    let status = `== Round ${i + 1} ==`;
    logStatus(status, dataObject);
    playGame(players, dataObject);
  }

  fs.writeFile("./result.json", JSON.stringify(dataObject, null, 2), function (err) {
    if (err) {
      console.log(err);
    }
  });
}

function ask(question, dataObject) {
  let answer = readlineSync.question(question);
  writeJson(question + answer, dataObject);
  return answer;
}

function logStatus(status, dataObject) {
  console.log(status);
  writeJson(status, dataObject);
}

function writeJson(status, dataObject) {
  // get object keys as array
  let keyArr = Object.keys(dataObject);
  // {row: text}
  dataObject[keyArr.length + 1] = status;
}

function createPlayer(dataObject) {
  let players = [];

  while (true) {
    let playerNumber = players.length + 1;
    let question = `Enter player ${playerNumber} name (enter nothing to stop): `;
    let name = ask(question, dataObject);
    if (name == "") {
      break;
    }
    // or {name, score: 0}
    players.push({ name: name, score: 0 });
  }
  logStatus(`Here are the player names and their scores: ${buildPlayerStatus(players)}`, dataObject);
  return players;
}

function playGame(players, dataObject) {
  // 0-5.xxxxx
  let randomNumber = Math.random() * 6;
  // 1 - 6
  let result = Math.floor(randomNumber) + 1;
  for (let player of players) {
    let question = `${player.name}'s guess the number (Big, Small): `;
    let guess = ask(question, dataObject);
    // 1-3 = small, 4-6 = big
    if ((result > 3 && guess.toLowerCase().trim() == "big") || (result < 4 && guess.toLowerCase().trim() == "small")) {
      player.score++;
    }
  }
  logStatus(`The result is: ${result}`, dataObject);
  logStatus(players, `The scores: ${buildPlayerStatus(players)}`, dataObject);
}

function buildPlayerStatus(players) {
  return players.map((player) => `${player.name} (${player.score})`).join(", ");
}
