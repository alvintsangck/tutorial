let data = {
  routes: [
    {
      overnightRoute: 0,
      routeId: "2",
      routeName_c: "大澳 > 梅窩碼頭",
      routeName_e: "Tai O > Mui Wo Ferry Pier",
      routeName_s: "大澳 > 梅窝码头",
      routeNo: "1",
      specialRoute: 0,
    },
    {
      overnightRoute: 0,
      routeId: "1",
      routeName_c: "梅窩碼頭 > 大澳",
      routeName_e: "Mui Wo Ferry Pier > Tai O",
      routeName_s: "梅窝码头 > 大澳",
      routeNo: "1",
      specialRoute: 0,
    },
    {
      overnightRoute: 0,
      routeId: "3",
      routeName_c: "紅磡 (紅鸞道)  > 昂坪",
      routeName_e: "Hung Hom (Hung Luen Road)  > Ngong Ping",
      routeName_s: "红磡 (红鸾道)  > 昂坪",
      routeNo: "1R",
      specialRoute: 0,
    },
  ],
};

// Further last question, can you create a function to:

// 1. Extract the route numbers using the data as a parameter
function getRouteNumbers(data) {
  let routes = data.routes;
  // All the route numbers
  let routeNumbers = [];
  for (let i = 0; i < routes.length; i++) {
    routeNumbers.push(routes[i].routeNo);
  }
  // console.log(routeNumbers);
  return routeNumbers;
}

console.log(getRouteNumbers(data));

// 2. Extract the terminal names of both ends of a particular route, using the data and route number as parameters
function getTerminalNames(data, routeNo) {
  let routes = data.routes;
  let terminalNames = [];
  for (let route of routes) {
    if (route.routeNo == routeNo) {
      let name = route.routeName_e;
      let nameArr = name.split(">");
      for (let name of nameArr) {
        terminalNames.push(name.trim());
      }
    }
  }
  return terminalNames;
}

console.log(getTerminalNames(data, "1R"));
// To test whether you have successfully created the functions, copy and paste the data from https://rt.data.gov.hk/v1/transport/nlb/route.php?action=list

// For json beautifier
// https://codebeautify.org/jsonviewer
