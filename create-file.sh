js_f2="js-f2"
js_f4="js-f4"
array=("$js_f2" "$js_f4")
js_f2_files=8
js_f4_files=9

create_file () {
    for ((i = 0; i < $2; i++ ))
    do
        num=$(($i + 1))
        file="$1/q$num.js"
        if [ ! -e "$file" ] ; then
            touch "$file"
        fi
    done
}

for element in "${array[@]}"
do
    path="${HOME}/code/$element"
    mkdir -p $path
    case "$element" in
        "$js_f2") create_file $path $js_f2_files
        ;;
        "$js_f4") create_file $path $js_f4_files
        ;;
    esac
done

